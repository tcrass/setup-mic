#!/bin/bash

trap 'kill $(jobs -p) > /dev/null' EXIT

default_channels=2
default_sample_rate=48000
default_mic_name=mic
default_monitor_name=monitor

input_sample_rate=48000
output_sample_rate=48000

arecord -l
while [ -z "$input_card_number" ]; do
    read -p "Enter input card number:          " input_card_number
done

read -p "Enter no of input channels [$default_channels]:   " input_channels
if [ -z "$input_channels" ]; then
    input_channels=$default_channels
fi
read -p "Enter input sample_rate [$default_sample_rate]:  " input_sample_rate
if [ -z "$input_sample_rate" ]; then
    input_sample_rate=$default_sample_rate
fi
read -p "Enter mic socket name [$default_mic_name]:      " mic_name
if [ -z "$mic_name" ]; then
    mic_name=$default_mic_name
fi


aplay -l
read -p "Enter output card number [$input_card_number; Ctrl+D for none]: " output_card_number
if [ "$?" -eq "0" ]; then
    if [ -z "$output_card_number" ]; then
        output_card_number=$input_card_number
    fi
    read -p "Enter no of output channels [$default_channels]:     " output_channels
    if [ -z "$output_channels" ]; then
        output_channels=$default_channels
    fi

    read -p "Enter output sample_rate [$default_sample_rate]:    " output_sample_rate
    if [ -z "$output_sample_rate" ]; then
        output_sample_rate=$default_sample_rate
    fi
    read -p "Enter monitor socket name [$default_monitor_name]: " monitor_name
    if [ -z "$monitor_name" ]; then
        monitor_name=$default_monitor_name
    fi

    alsa_out -j "$monitor_name" -d hw:$output_card_number -c $output_channels -r $output_sample_rate &
    #zita-j2a -j monitor -d hw:$output_card_number -c $output_channels -r $output_sample_rate &
fi

alsa_in -j "$mic_name" -d hw:$input_card_number -c $input_channels -r $input_sample_rate &
#zita-a2j -j mic -d $input_card_number -c $input_channels & -r $input_sample_rate &

echo
read -p "Hit [Enter] to quit." $quit
